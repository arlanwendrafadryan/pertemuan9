/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan9.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    requires: [
        'Ext.grid.plugin.Editable',
    ],
    plugins: [{
        type: 'grideditable'
    },
    ],

    title: 'List Bar',
    bind: '{bar}',

    viewModel: {
        stores: {
           bar: {
                type: 'bar'
            }
        }
    },

    columns: [
        { text: 'Name', dataIndex: 'name', width: 250, editable: true},
        { text: 'G1', dataIndex: 'g1', width: 230, editable: true},
        { text: 'G2', dataIndex: 'g2', width: 150, editable: true },
        { text: 'G3', dataIndex: 'g3', width: 150, editable: true },
        { text: 'G4', dataIndex: 'g4', width: 150, editable: true },
        { text: 'G5', dataIndex: 'g5', width: 150, editable: true },
        { text: 'G6', dataIndex: 'g6', width: 150, editable: true },
    ],

    // listeners: {
    //     select: 'onBarItemSelected'
    // }
});
