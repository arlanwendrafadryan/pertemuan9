/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan9.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan9.view.main.MainController',
        'Pertemuan9.view.main.MainModel',
        'Pertemuan9.view.main.List',
        'Pertemuan9.view.main.BasicDataView',
        'Pertemuan9.view.main.Form',
        'Pertemuan9.store.Bar',
        'Pertemuan9.view.main.Bar',
        'Pertemuan9.view.main.ListBar',
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            items: [
                {
                    xtype: 'button',
                    text: 'Data',
                    ui: 'action',
                    scope: this,
                    listeners: {
                        tap: 'onReadClicked'
                    }
                },
            ]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items: [{
                    xtype: 'mainlist',
                    flex: 2
                },
                {
                    xtype: 'detail',
                    flex: 1
                },
                {
                    xtype: 'editform',
                    flex: 1
                }]
            }]
        }, {
            title: 'Chart',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items: [{
                    xtype: 'bar',
                    flex: 1
                },
                {
                    xtype: 'listbar',
                    flex: 1
                },]
            }]
        }, {
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            bind: {
                html: '{loremIpsum}'
            }
        }, {
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
