    Ext.define('Pertemuan9.store.Bar', {
        extend: 'Ext.data.Store',
        alias: 'store.bar',
        autoLoad: true,
        autoSync: true,

        fields: ['bar_id', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'name'],

        proxy: {
            type: 'jsonp',
            api: {
                read: "http://localhost/MyApp_php/readBar.php",
                update: "http://localhost/MyApp_php/updateBar.php",
                // destroy: "http://localhost/MyApp_php/destroypersonnel.php"
            },
            reader: {
                type: 'json',
                rootProperty: 'items' 
            }
        },
      
    });
