Ext.define('Pertemuan9.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId:'personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'user_id','name', 'email', 'phone'
    ],

   

    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/readpersonnel.php",
            update: "http://localhost/MyApp_php/updatepersonnel.php",
            destroy: "http://localhost/MyApp_php/destroypersonnel.php",
            create : "http://localhost/MyApp_php/createpersonnel.php",
        },
        reader: {
            type: 'json',
            rootProperty: 'items' 
        }
    },
    // listeners:{
    //     beforeload: function(store, operation, e0pts){
    //         this.getProxy().setExtraParams({
    //             user_id: -1
    //         });
    //     }
    // }
});
